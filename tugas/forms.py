from django import forms

from .models import Friend,Year

class friendForm(forms.ModelForm):

    class Meta:
        model = Friend
        fields = ('name','hobby','favorite_food','favorite_drink','year',)


##Create your forms here