from django.shortcuts import render,redirect
from .forms import friendForm
from .models import *
# Create your views here.
def home(request):
    return render(request,'pages/index.html',{'name':'Home Page'})

def about(request):
    return render(request,'pages/about.html',{'name':'About Page'})

def contact(request):
    if request.method == "POST":
         form = friendForm(request.POST)
         if form.is_valid():
             form.save()
         return redirect('friend')
    else:
        form = friendForm()
    return render(request,'pages/contact.html',{'name':'Contact Page','form':form})
def delete_friend(request,pk):
    Friend.objects.filter(id=pk).delete()
    return redirect('friend')
def friend(request):
    friend = Friend.objects.all()
    year = Year.objects.all()
    return render(request,'pages/friend.html',{'name':'Friends Page','friends':friend,'years':year})