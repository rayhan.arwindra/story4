from django.db import models
class Year (models.Model):
    year = models.CharField(max_length=4)
    
    def __str__ (self):
        return self.year

class Friend(models.Model):
    name = models.CharField(max_length=200)
    hobby = models.CharField(max_length=200)
    favorite_food = models.CharField(max_length=200)
    favorite_drink = models.CharField(max_length=200)
    year = models.ForeignKey(Year, on_delete=models.CASCADE)

    def __str__ (self):
        return self.name
# Create your models here.
