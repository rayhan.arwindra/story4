from django.urls import path
from . import views

urlpatterns = [
    path('',views.home,name='home'),
    path('about/',views.about,name='about'),
    path('contact/',views.contact,name='contact'),
    path('friend/',views.friend,name='friend'),
    path(r'^delete_friend/(?P<pk>\d+)/$', views.delete_friend, name='delete_friend'),
]