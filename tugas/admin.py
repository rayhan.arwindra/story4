from django.contrib import admin
from . import models

admin.site.register(models.Friend)
admin.site.register(models.Year)
# Register your models here.